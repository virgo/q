var trname =[];
trname.AACASAD = "Muhammad Asad"; 
trname.AADPKTL = "M. M. Pickthall"; 
trname.SHKR = "Shakir";
trname.AAEYF85 = "Yusuf Ali (Saudi Rev. 1985)";
trname.AAFYF38 = "Yusuf Ali (Orig. 1938)";
trname.BKTR = "Dr. Laleh Bakhtiar";
trname.WHDN = "Wahiduddin Khan";
trname.IVNG = "T.B.Irving";
trname.MNKB = "Al-Muntakhab";
trname.MN11 = "[The Monotheist Group] (2011 Edition)";
trname.AABHALM = "Abdel Haleem";
trname.AAGDRBD = "Abdul Majid Daryabadi";
trname.AALI = "Ahmed Ali";
trname.BWLY = "Aisha Bewley";
trname.AUNL = "Ali Unal";
trname.QQRI = "Ali Quli Qarai";
trname.HMID = "Hamid S. Aziz";
trname.GHLI = "Muhammad Mahmoud Ghali";
trname.SRWR = "Muhammad Sarwar";
trname.AAHUMNI = "Muhammad Taqi Usmani";
trname.SHBR = "Shabbir Ahmed";
trname.VQKR = "Syed Vickar Ahamed";
trname.AAASAHI = "Umm Muhammad (Sahih International)";
trname.MALK = "Farook Malik";
trname.MNSH = "Dr. Munir Munshey";
trname.QDRI = "Dr. Mohammad Tahir-ul-Qadri";
trname.OMAR = "Dr. Kamal Omar";
trname.ITNI = "Talal A. Itani (new translation)";
trname.BLAL = "Bilal Muhammad (2013 Edition)";
trname.AAIMDDI = "Maududi";
trname.MN13 = "[The Monotheist Group] (2013 Edition)";
trname.BJAN = "Bijan Moeinian";
trname.FHAQ = "Faridul Haque";
trname.QRBL = "Hasan Al-Fatih Qaribullah";
trname.MALI = "Maulana Muhammad Ali";
trname.SMRA = "Muhammad Ahmed - Samira";
trname.SHRL = "Sher Ali";
trname.RSHD = "Rashad Khalifa";
trname.AAJRAZA = "Ahmed Raza Khan (Barelvi)";
trname.AMTL = "Amatul Rahman Omar";
trname.HLLI = "Muhsin Khan & Muhammad al-Hilali";
trname.ARBY = "Arthur John Arberry";
trname.PLMR = "Edward Henry Palmer";
trname.SALE = "George Sale";
trname.RDWL = "John Medows Rodwell";

angular.module('thisApp', ['ui.bootstrap']);

angular.module('thisApp')
  .controller('MainCtrl', ['$scope', '$http', '$modal', '$log', '$filter', function($scope, $http, $modal, $log, $filter) {
    'use strict';
    $scope.searchResponse = {};
    $scope.trname = trname;
    $scope.ayatID = 1;
    $scope.surah = 1;
    $scope.ayat = 1;
    $scope.translator = ["AAASAHI", "AABHALM", "AACASAD", "AADPKTL", "AAEYF85", "AAFYF38", "AAGDRBD", "AAHUMNI", "AAIMDDI", "AAJRAZA"];
    //$scope.translator = ["ASAD","PKTL","SHKR","YF85","YF38","BKTR","WHDN","IVNG","MNKB","MN11","HALM","DRBD","AALI","BWLY","AUNL","QQRI","HMID","GHLI","SRWR","UMNI","SHBR","VQKR","SAHI","MALK","MNSH","QDRI","OMAR","ITNI","BLAL","MDDI","MN13","BJAN","FHAQ","QRBL","MALI","SMRA","SHRL","RSHD","RAZA","AMTL","HLLI","ARBY","PLMR","SALE","RDWL","DWOD","QUTB","HANF","SDRM","ANSD","SHFI"];

    $scope.arabicURL = "/static/img/1_1.png";
    $scope.custom = "reader";
    $scope.translit = "Bismi Allahi alrrahmani alrraheemi"
    $scope.arabictext = "بِسمِ اللَّهِ الرَّحمٰنِ الرَّحيمِ الم"

    $scope.singleModel = 1;
    $scope.radioModel = 'Middle';

    $scope.checkModel = {
      left: false,
      middle: true,
      right: false
    };

    $scope.getSurahAyatObj = function(verse) {
        var surah = arraySearch(surahStarts, verse);
        var ayah = verse - detail(surah).start;
        return{
            surah: surah,
            ayah: ayah
        };
    };


    function fetch(){
      $scope.surah = $scope.getSurahAyatObj($scope.ayatID).surah;
      $scope.ayat =  $scope.getSurahAyatObj($scope.ayatID).ayah;

      $scope.arabicURL = "/static/img/"+$scope.surah+"_"+$scope.ayat+".png";

      $http.get('/v1/search/'+$scope.surah +'/'+ $scope.ayat).then(function(res) {
        var arr = JSON.parse(res.data);
        var arrT = {};
        for (i=0; i< ($scope.translator).length; i++) {
          arrT[($scope.translator)[i]] = arr[($scope.translator)[i]];   
        }
        $scope.searchResponse = arrT;
        $scope.custom = "reader";
      }, function(msg) {
          console.log(msg);
      });

      $http.get('/v1/transbyverse/'+$scope.ayatID).then(function(res) {
        var r = JSON.parse(res.data);
        var arr = r.split("<br>");
        $scope.translit = arr[1];
        $scope.arabictext = arr[0];
      }, function(msg) {
          console.log(msg);
      });
    }
    var pathArray = window.location.pathname.split('/');
    if (pathArray[1] == 'a' && !isNaN(pathArray[2]) && !isNaN(pathArray[3])) {
      $scope.surah = pathArray[2];
      $scope.ayat = pathArray[3];
      $scope.ayatID = ayah(pathArray[2], parseInt(pathArray[3]));
      fetch();
    } else {
      $scope.surah = 1
      $scope.ayat = 1
    }


    $http.get('/v1/search/' +$scope.surah +'/'+ $scope.ayat).then(function(res) {
      $scope.searchResponse = JSON.parse(res.data);        
      $scope.custom = reader;     
      $scope.custom = "reader";
    }, function(msg) {
        
    });

    $scope.filterTranslators = function(item) {
        console.log(item);
        return true;
    };

    $scope.next = function() {
        $scope.ayatID =  $scope.ayatID + 1;
        fetch();
      
    };

    $scope.prev = function() {
        $scope.ayatID =  $scope.ayatID - 1;
        fetch();
    };

    $scope.change = function() {
        var arr = ($scope.surahayat).split(":");
        if(arr.length === 2 && arr[0] != "" && arr[1] != "" && parseInt(arr[0]) && parseInt(arr[1]) )
        {
            $scope.surah = parseInt(arr[0])
            $scope.ayat =  parseInt(arr[1])
            $scope.ayatID = ayah($scope.surah, $scope.ayat)
            $scope.arabicURL = "/static/img/"+$scope.surah+"_"+$scope.ayat+".png"
            
            $http.get('/v1/search/'+$scope.surah +'/'+ $scope.ayat).then(function(res) {
                var arr = JSON.parse(res.data);
                var arrT = {};
                for (i=0; i< ($scope.translator).length; i++) {
                    arrT[($scope.translator)[i]] = arr[($scope.translator)[i]];   
                }
                $scope.searchResponse = arrT;
                $scope.custom = "reader";
            }, function(msg) { 
            });

            $http.get('/v1/transbyverse/'+$scope.ayatID).then(function(res) {
                var r = JSON.parse(res.data);
                var arr = r.split("<br>");

                $scope.translit = arr[1];
                $scope.arabictext = arr[0];
                }, function(msg) {
                console.log(msg);
            });
        }
    };

    $scope.qsearch = function() {
        $scope.arabicURL = "/static/img/q_0.png"
        $http.get('/v1/qsearch/'+$scope.surahayat).then(function(res) {
            var arr = JSON.parse(res.data);
            var arrT =[];
            for (i=0; i< arr.length; i++) {
                if (($scope.translator).indexOf(arr[i]["Translator"]) >= 0){ 
                   arrT.push(arr[i]);
                }
            }
            $scope.searchResponse = arrT;
            $scope.custom = "search";

        }, function(msg) { 
        });        
    };  

    $scope.getSurahAyat = function(verse) {
        var surah = arraySearch(surahStarts, verse);
        var ayah = verse - detail(surah).start;
        return surah + ":" + ayah
    };

    

    $scope.open = function (size) {
      var items = [$scope.trname, $scope.translator];
      var modalInstance = $modal.open({
         templateUrl: "myModalContent.html",
         controller: "ModalInstanceCtrl",
         size: size,
         resolve: {
            items: function () {
               return items;
            }
         }
      });

      modalInstance.result.then(function (items) {
         $scope.translator = [];
         for(i=0; i<items.length; i++){
            if (items[i].check){
               ($scope.translator).push(items[i].key);   
            }
         }
         // var arr = $scope.searchResponse;
         //    var arrT = {};
         //    for (i=0; i< ($scope.translator).length; i++) {
         //        arrT[($scope.translator)[i]] = arr[($scope.translator)[i]];   
         //    }
         //    $scope.searchResponse = arrT;

      }, function () {
         $log.info("Modal dismissed at: " + new Date());
      });
   };

}]);

// $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

angular.module("thisApp").controller("ModalInstanceCtrl", function ($scope, $modalInstance, items) {

var transpopmodel = [];
  for (var property in items[0]) {
    if (items[0].hasOwnProperty(property)) {
         var p = {}
         p.name = items[0][property];
         p.key = property;
         if ((items[1]).indexOf(property) >= 0){
            p.check = true;
         } else {
            p.check = false;   
         }
        transpopmodel.push(p);
    }
} 

  $scope.items = transpopmodel;

  $scope.ok = function () {
    $modalInstance.close($scope.items);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});    

