package models

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"runtime"
	"strconv"
	"strings"
)

var AyatIDMap map[string]int           //[ayat:verse]ayatID
type Translations map[string]string    //[translator]verse
var GlobalQuran = [6237]Translations{} //[ayatID]map of [translator]verse
var ArabicQuran [6237]string           //[ayatID]arabic verse
var TransQuran [6237]string            //[ayatID]translitered verse

type JSQuran struct {
	AyatID      int
	Translator  string
	Translation string
	ArabicText  string
}

func init() {
	AyatIDMap = make(map[string]int)
	ArabicQuran[0] = "start"
	TransQuran[0] = "start"
	GlobalQuran = makeVerseArray()
	loadArabicQuran()
	loadTransQuran()
	PrintMemStats()
}

func makeVerseArray() [6237]Translations {
	var arr [6237]Translations
	file, err := os.Open("resources/whole.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	var t Translations
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		v := strings.Split(scanner.Text(), "||")
		currentid, _ := strconv.Atoi(v[0])

		t = arr[currentid]

		if t == nil {
			t = make(map[string]string)
			t[v[3]] = v[4]
			arr[currentid] = t
			s := v[1] + ":" + v[2]
			AyatIDMap[s] = currentid

		} else {
			t[v[3]] = v[4]
		}

	}

	return arr
}

func loadArabicQuran() {
	file, err := os.Open("resources/quran.txt")
	if err != nil {
		log.Fatal(err)
	}
	i := 1
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		v := strings.Split(scanner.Text(), "|")
		ArabicQuran[i] = v[2]
		i = i + 1
	}
}

func loadTransQuran() {
	file, err := os.Open("resources/transliteration.txt")
	if err != nil {
		log.Fatal(err)
	}
	i := 1
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		TransQuran[i] = scanner.Text()
		i = i + 1
	}
}

func PrintMemStats() {
	ms := runtime.MemStats{}
	runtime.ReadMemStats(&ms)
	fmt.Println(ms.Sys / 1024 / 1024)
}
