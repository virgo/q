<!doctype html>
<html ng-app="thisApp">

<head>
    <title>Project Q</title>
    <script src="../../static/js/angular.min.js"></script>
    <script src="../../static/js/ui-bootstrap-tpls-0.11.2.min.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../../static/js/quran.js"></script>
    <script src="../../static/js/underscore.js"></script>
    <script src="../../static/js/app.js"></script>
    <link href="../../static/css/bootstrap_v3.2.0.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../static/css/home.css">
</head>

<body >
    <div class="container" ng-controller="MainCtrl">
        <div style="float:left;margin-top:15px;">
            <button type="button" class="btn btn-primary" ng-click="open()"> Translators </button> 
        </div>    
        <div style="float:right;margin-top:15px;">
            <button ng-click="prev()" style="float:left" type="button" class="btn btn-primary"> Prev </button> 
            <div style="float:left;padding:5px; color: #006DAC;font-weight: bold;">{{surah}}:{{ayat}}</div>
            <button ng-click="next()" type="button" class="btn btn-primary"> Next </button>
        </div>
        <h3 class="text-center">Project Q</h3>
        <div style="text-align:center">
            <input type="text" ng-model="surahayat" ng-change="change()" style="height: 40px;font-size: 20px;width:60%" >  <button ng-click="qsearch()" type="button" class="btn btn-warning"> Search </button>
        </div>
        <div ng-show="custom == 'reader'" id="reader" style="margin-top:15px">
             <div style="margin-bottom: 15px;font-size: 25px">{{arabictext}}</div>
             <div style="font-style: italic; margin-bottom: 25px;font-size: 20px">{{translit}}</div>
            <ul ng-repeat="(key, value) in searchResponse | filter:filterTranslators" style="list-style-type: none; padding-left: 0;">
                <li change class="searchitem">
                    <div class="translatorHeading"><b>{{trname[key]}}</b></div>
                    <div>{{value}}</div>
                </li>
            </ul>
        </div>

        <div ng-show="custom == 'search'" id="searchresultsdiv">
            <span>Total Verses : {{searchResponse.length}}</span>
            <ul ng-repeat="item in searchResponse | filter:filterTranslators" style="list-style-type: none;">
                <li change class="searchitem">
                    <div style="float:left" class="verseHeading"><b>{{getSurahAyat(item.AyatID)}}</b></div>
                    <div class="translatorHeading"><b>&nbsp;&nbsp;{{trname[item.Translator]}}</b></div>
                    <div>{{item.Translation}}</div>
<!--                     <div style="font-size:25px">{{item.ArabicText}}</div>
 -->                </li>
            </ul>
        </div>
   
    </div>
</div>
<div class="errors">
    <p ng-repeat="e in errors" ng-click="errors.splice($index, 1)">{{e}}</p>
</div>


<div>
    <script type="text/ng-template" id="myModalContent.html">
        <div class="modal-header">
            <h4 class="modal-title" style="float:left">Choose Translators</h4>         
        </div>
        <div class="modal-body">
            <div ng-repeat="item in items" style="float:left; padding:2px;">
            <label class="btn btn-default btn-xs"  ng-model="item.check" btn-checkbox>{{item.name}}</label>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary pull-right" ng-click="ok()">OK</button>
            <button class="btn btn-warning pull-right" ng-click="cancel()">Cancel</button> 

        </div>
    </script>

    <!-- <button class="btn btn-default" ng-click="open()">Open me!</button>
    <button class="btn btn-default" ng-click="open('lg')">Large modal</button>
    <button class="btn btn-default" ng-click="open('sm')">Small modal</button>
    <div ng-show="selected">Selection from a modal: {{ selected }}</div> -->
</div>



</body>
</html>