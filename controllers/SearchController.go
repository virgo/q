package controllers

import (
	"encoding/json"
	"log"
	"qproject/models"
	"qproject/virgo"
	"strconv"
	"strings"
	"sync"
)

var ayatIDMap map[string]int       //[ayat:verse]ayatID
type ayatTranslator map[int]string //[ayatID]translator

type searchresults struct {
	AyatID      int
	Translator  string
	Translation string
	ArabicText  string
}

type SearchController struct {
	virgo.Controller
}

func (u *SearchController) Get() {
	searchstring := u.Ctx.Input.Params["searchstring"]

	wg := &sync.WaitGroup{}

	hitsChannel := make(chan ayatTranslator) //[ayatID]translator
	wg.Add(6236)
	for i := 1; i <= 6236; i++ {
		go Search(wg, hitsChannel, searchstring, i)
	}

	go monitorSearch(wg, hitsChannel)

	totalHits := 0
	sr := make([]searchresults, 0)

	for result := range hitsChannel {
		for key, val := range result {
			sr = append(sr, searchresults{
				AyatID:      key,
				Translator:  val,
				Translation: models.GlobalQuran[key][val],
				ArabicText:  models.ArabicQuran[key],
			})
		}

		totalHits = totalHits + len(result)
	}

	j, err := json.Marshal(sr)
	if err != nil {
		log.Fatal(err)
	}
	u.Data["json"] = string(j)
	//fmt.Println(string(j), totalHits)

	//u.Data["json"] = `{"Score":` + searchstring + `+,"PlayerName":"Sean Plott"}'`

	u.ServeJson()
	models.PrintMemStats()
}

func monitorSearch(wg *sync.WaitGroup, hitsChannel chan ayatTranslator) {
	wg.Wait()
	close(hitsChannel)
}

func Search(wg *sync.WaitGroup, hitsChannel chan ayatTranslator, token string, ayatID int) {
	defer wg.Done()
	m := models.GlobalQuran[ayatID]
	for k, v := range m {
		if strings.Contains(toLower(v), token) {
			at := make(ayatTranslator)
			at[ayatID] = k
			hitsChannel <- at
		}
	}
}

func (u *SearchController) TransByVerse() {
	a, err := strconv.Atoi(u.Ctx.Input.Params["verseid"])
	if err != nil {
		log.Fatal(err)
	}
	x, err := json.Marshal(models.ArabicQuran[a] + "<br>" + models.TransQuran[a])
	if err != nil {
		log.Fatal(err)
	}
	u.Data["json"] = string(x)
	u.ServeJson()
	models.PrintMemStats()
}

func toLower(s string) string {
	b := make([]byte, len(s))
	for i := range b {
		c := s[i]
		if c >= 'A' && c <= 'Z' {
			c += 'a' - 'A'
		}
		b[i] = c
	}
	return string(b)
}
