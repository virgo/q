package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"qproject/models"
	"qproject/virgo"
)

type MainController struct {
	virgo.Controller
}

func (u *MainController) Get() {
	ayatverse := u.Ctx.Input.Params["ayat"] + ":" + u.Ctx.Input.Params["verse"]
	a := models.GlobalQuran[models.AyatIDMap[ayatverse]]
	j, err := json.Marshal(a)
	if err != nil {
		log.Fatal(err)
	}
	u.Data["json"] = string(j)

	// u.Data["json"] = `{"Groups": [{"Name": "GoSV", "URL": "http://www.meetup.com/golangsv", "Members": 194, "City": "San Mateo", "Country": "US"}, {"Name": "GoSF", "URL": "http: //www.meetup.com/golangsf", "Members": 1393, "City": "San Francisco", "Country": "US"}], "Errors": ["something bad happened"] }`

	u.ServeJson()
	models.PrintMemStats()
}

func (u *MainController) ServeHome() {
	fmt.Println("Here.....")
	u.Template = "index.tpl"
	models.PrintMemStats()
}

func (u *MainController) ServeHomeWithAyats() {
	fmt.Println("Here.....")
	ayatverse := u.Ctx.Input.Params["a"] + ":" + u.Ctx.Input.Params["v"]
	a := models.GlobalQuran[models.AyatIDMap[ayatverse]]
	j, err := json.Marshal(a)
	if err != nil {
		log.Fatal(err)
		log.Fatal(j)
	}
	//log.Println(j)
	u.Template = "index.tpl"
	models.PrintMemStats()
}

func (u *MainController) GetAyatVerse() {
	fmt.Println("GetAyatVerse.....")

	u.Data["json"] = "asdfasdfasdfasdfasdfasd"
	u.ServeJson()

	models.PrintMemStats()
}
