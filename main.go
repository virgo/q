package main

import (
	"fmt"
	"qproject/controllers"
	_ "qproject/models"

	"qproject/virgo"
)

func main() {
	fmt.Println("Starting....")
	virgo.Start()
}

func init() {
	virgo.AddRoute("/", &controllers.MainController{}, "get:ServeHome")
	virgo.AddRoute("/a/:a/:v", &controllers.MainController{}, "get:ServeHomeWithAyats")
	virgo.AddRoute("/v1/search/:ayat/:verse", &controllers.MainController{})
	virgo.AddRoute("/v1/qsearch/:searchstring", &controllers.SearchController{})
	virgo.AddRoute("/v1/transbyverse/:verseid", &controllers.SearchController{}, "get:TransByVerse")
}
